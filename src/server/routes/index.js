var router = require('express').Router();
const blockchain = require('../blockchain');

router.get('/', (req, res) => {
  res.send(`Blockchain Connected: ${blockchain.isConnected}`);
});

router.get('/api/tokens', (req, res) => {
  res.json(blockchain.allTokensInfo);
});

module.exports = router;