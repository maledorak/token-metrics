const config = require('./config');


module.exports = Object.freeze({
  INFURA_BLOCKCHAIN_URL: `https://mainnet.infura.io/v3/${config.blockchain.infuraApiKey}`,
  ADDRESSES_OF_TOKENS: Object.freeze({
    BNB: '0xB8c77482e45F1F44dE1745F52C74426C631bDD52',
    VEN: '0xd850942ef8811f2a866692a623011bde52a462c1',
    OMG: '0xd26114cd6EE289AccF82350c8d8487fedB8A0C07',
    ZRX: '0xe41d2489571d322189246dafa5ebde1f4699f498',
    ZIL: '0x05f4a42e251f2d52b8ed15e9fedaacfcef1fad27',
    BAT: '0x0d8775f648430679a709e98d2b0cb6250d2887ef'
  })
});
