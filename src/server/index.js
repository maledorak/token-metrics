const express = require('express');
const app = express();

const config = require('./config');

app.use(express.static('dist'));
app.use(require('./routes'));

/// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.listen(config.server.port, () => {
  return console.log(`Server is listening on port ${config.server.port}`)
});
