require('dotenv').config();
const env = process.env.NODE_ENV || 'dev';

// Ensure required ENV vars are set
let requiredEnv = [
  'INFURA_API_KEY'
];

let unsetEnv = requiredEnv.filter((env) => {
  return (
    !(typeof process.env[env] !== 'undefined') || (process.env[env].length === 0)
  )
});

if (unsetEnv.length > 0) {
  const unsetEnvList = unsetEnv.join(', ');
  throw new Error(`Required ENV variables are not set: [${unsetEnvList}]`);
}

// Config
const dev = {
  server: {
    port: process.env.PORT || 3100
  },
  blockchain: {
    infuraApiKey: process.env.INFURA_API_KEY
  },
};

const test = {
  server: {
    port: process.env.PORT || 3100
  },
  blockchain: {
    infuraApiKey: process.env.INFURA_API_KEY
  },
};

const index = {
  dev,
  test
};

module.exports = index[env];