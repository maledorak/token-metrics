let getContracts = (web3, addresses) => {
  const abi = require('human-standard-token-abi');
  Object.keys(addresses).map((key, index) => {
    addresses[key] = web3.eth.contract(abi).at(addresses[key]);
  });
  return addresses
};

let getTokenInfo = (contract) => {
  return {
    name: contract.name(),
    symbol: contract.symbol(),
    totalSupply: contract.totalSupply()
  };
};

let getAllTokensInfo = (contracts) => {
  let infos = Object.values(contracts);
  for (var i = 0, len = infos.length; i < len; i++) {
    console.log(`Loading info contract of ${i+1}/${infos.length}`);
    infos[i] = getTokenInfo(infos[i]);
  }
  return infos
};

module.exports = {
  getContracts,
  getAllTokensInfo
};
