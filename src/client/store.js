import {applyMiddleware, createStore} from 'redux';
import {createLogger} from 'redux-logger'
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly';
import reducer from './reducer';

const getMiddleware = () => {
  return applyMiddleware(createLogger(), thunk)
};

export const store = createStore(
  reducer, composeWithDevTools(getMiddleware()));
