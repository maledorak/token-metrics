import React, {Fragment} from "react"
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import compose from 'recompose/compose'
import {Grid} from "@material-ui/core";
import {withStyles} from '@material-ui/core/styles';

import TokenCard from "../../components/TokenCard/index";
import {getTokens} from './actions'


const styles = {
  gridItem: {
    padding: 10,
  },
};

const mapStateToProps = state => ({
  ...state.tokens,
});

const mapDispatchToProps = dispatch => ({
  getTokens: () => dispatch(getTokens())
});

class Tokens extends React.Component {
  componentDidMount() {
    this.props.getTokens()
  }

  render() {
    const {classes, tokens} = this.props;
    if (this.props.isLoading) {
      return (<p>Loading...</p>);
    }
    return (
      <Fragment>
        <Grid container direction="row" alignItems="flex-start" justify="space-around">
          {tokens.length > 0 ? tokens.map((tokenItem, index) => {
            return (
              <Grid className={classes.gridItem} key={tokenItem.symbol} item xs={12} sm={6} md={4} xl={2}>
                <TokenCard key={tokenItem.symbol} token={tokenItem}/>
              </Grid>
            )
          }) : <p>There is no tokens</p>}
        </Grid>
      </Fragment>
    )
  }
}

Tokens.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Tokens)
