import fetch from 'isomorphic-fetch';
import {tokensTypes} from '../../constants/actionTypes'


export function getTokensRequested() {
  return {
    type: tokensTypes.GET_TOKENS_REQUESTED
  };
}

export function getTokenDone(data) {
  return {
    type: tokensTypes.GET_TOKENS_DONE,
    payload: data
  };
}

export function getTokenFailed(error) {
  return {
    type: tokensTypes.GET_TOKENS_FAILED,
    payload: error
  };
}

export function getTokens() {
  return dispatch => {
    // set state to "loading"
    dispatch(getTokensRequested());

    fetch('/api/tokens')
      .then(response => response.json())
      .then(data => {
        // set state for success
        dispatch(getTokenDone(data));
      })
      .catch(error => {
        // set state for error
        dispatch(getTokenFailed(error));
      })
  }
}
