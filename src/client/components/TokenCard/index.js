import React from 'react'
import PropTypes from 'prop-types';
import {Card, CardContent, CardHeader, Typography} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';


const styles = {
  card: {
    height: 250,
  },
};

class TokenCard extends React.Component {
  render() {
    const {classes, token} = this.props;
    return (
      <Card className={classes.card}>
        <CardHeader title={token.symbol}/>
        <CardContent>
          <Typography component="p">Name: {token.name}</Typography>
          <Typography component="p">Total supply:{token.totalSupply}</Typography>
        </CardContent>
      </Card>
    )
  };
}

TokenCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TokenCard);
